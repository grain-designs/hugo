---
title: Prices
comments: false
---

We have a few services, take a look at the prices!


**Service** | **Starting Price** 
--- | ---
Logo | $5
Banner | $8
Thumbnail | $12
Discord Bot Creation | $9


For more information on each service, please visit our Services page <!-- TODO: Add link -->