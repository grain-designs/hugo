---
title: "Initial Post"
subtitle: "Welcome to Grain Designs!"
tags: ["update"]
date: "2019-01-02"
draft: true
---

Welcome to our new project! We try our hardest to meet your expectations for us, and to excel at what we do. You expect the best from
us, and we'll try to live up to that. Our services are cheap, but will prove their worth many times over. 

On behalf of the entire staff team, thank you for choosing Grain Designs, we hope you aren't disappointed.

## Services
We offer a number of services, from graphic art to programming.

- Logo
- Banner
- Thumbnail
- Discord Server Setup
- Discord Bot Creation

For more information, go to our [prices]({{< ref page/prices >}}) page!

### More Details
#### Discord Setup
This service, for just $2, can get you a professional Discord server with roles, channels, and bots, ready for use.
If you want more details, you can join our [Discord](https://discord.gg/E8jtva7) and ask to talk to a sales representative.